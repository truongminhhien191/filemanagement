import libs

libs.APP_ROOT = libs.path.join(libs.path.dirname(__file__), '.')   # refers to application_top
libs.UPLOAD_FOLDER = libs.APP_ROOT + '/uploads'
dotenv_path = libs.path.join(libs.APP_ROOT, '.env')
libs.load_dotenv(dotenv_path)

if not libs.path.exists(libs.UPLOAD_FOLDER):
    libs.makedirs(libs.UPLOAD_FOLDER)
web = libs.Blueprint('web', __name__, template_folder='templates')


@web.route('/', methods=['GET', 'POST'])
def upload_file():
    if libs.request.method == 'POST':
        
        if 'file' not in libs.request.files:
            libs.flash('Please select a file !')
            return libs.redirect(libs.request.url)

        file = libs.request.files['file']

        if file.filename == '':
            libs.flash('Please select a file !')
            return libs.redirect(libs.request.url)

        filename = libs.secure_filename(file.filename)
        
        if libs.is_same_name(filename, libs.UPLOAD_FOLDER):
            libs.flash('File is exist !')
            return libs.redirect(libs.request.url)

        file.save(libs.path.join(libs.UPLOAD_FOLDER, filename))
        ftmp = libs.is_same_content(filename, libs.UPLOAD_FOLDER)

        if ftmp:
            libs.remove(libs.path.join(libs.UPLOAD_FOLDER, filename))            
            libs.flash('File is same content  with ' + ftmp)
            return libs.redirect(libs.request.url)

        libs.flash('Upload successfully')
        return libs.redirect(libs.request.url)
    return libs.render_template('uploads.html', filelist=libs.get_lists_file(libs.UPLOAD_FOLDER) )


@web.route('/retrieve/<filename>')
def retrieve_file(filename):
    return libs.send_from_directory(libs.UPLOAD_FOLDER, filename)

@web.route('/delete', methods=['POST'])
def delete_file():
    pathfile = libs.path.join(libs.UPLOAD_FOLDER, libs.request.form['filename'])

    if libs.path.exists(pathfile):
        libs.flash('Delete successfully')
        libs.remove(pathfile)
    else:
        libs.flash("The file does not exist")

    return libs.redirect(libs.url_for('web.upload_file'))


@web.route('/<page>')
def show(page):
    try:
        return libs.render_template('%s.html' % page)
    except libs.TemplateNotFound:
        libs.abort(404)