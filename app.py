from web import web
from api import api
import libs

app = libs.Flask(__name__)
app.secret_key = libs.getenv("SECRET_KEY")
app.register_blueprint(web)
app.register_blueprint(api)

if __name__ == '__main__':
    app.run(host=libs.getenv("HOST"), port=libs.getenv("PORT"), debug=True)
