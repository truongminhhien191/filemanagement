import libs

libs.APP_ROOT = libs.path.join(libs.path.dirname(__file__), '.')   # refers to application_top
libs.UPLOAD_FOLDER = libs.APP_ROOT + '/uploads'
dotenv_path = libs.path.join(libs.APP_ROOT, '.env')
libs.load_dotenv(dotenv_path)

api = libs.Blueprint('api', __name__, template_folder='templates')


@api.route('/api/retrievelist')
def api_retrieve_list():
    return libs.jsonify({'list': libs.get_lists_file(libs.UPLOAD_FOLDER)}), 200


@api.route('/api/retrieve/<filename>')
def api_retrieve_file(filename):
    
    pathfile = libs.path.join(libs.UPLOAD_FOLDER, filename)
    
    if libs.path.exists(pathfile):
        return libs.send_from_directory(libs.UPLOAD_FOLDER, filename), 200
    
    return libs.jsonify({'status': 'File not Found !'}), 404


@api.route('/api/upload', methods=['GET', 'POST'])
def api_upload_file():
    if libs.request.method == 'POST':    

        if 'file' not in libs.request.files:
            return libs.jsonify({'status': 'Please select a file !'}), 400

        file = libs.request.files['file']

        if file.filename == '':
            return libs.jsonify({'status': 'Please select a file !'}), 400

        filename = libs.secure_filename(file.filename)

        if libs.is_same_name(filename, libs.UPLOAD_FOLDER):
            return libs.jsonify({'status': 'File is exist !'}), 409            
        
        file.save(libs.path.join(libs.UPLOAD_FOLDER, filename))
        ftmp = libs.is_same_content(filename, libs.UPLOAD_FOLDER)

        if ftmp:
            libs.remove(libs.path.join(libs.UPLOAD_FOLDER, filename))            
            return libs.jsonify({'status': 'File is same content  with ' + ftmp}), 409 

        return libs.jsonify({'status': 'Upload successfully !'}), 200            
    return libs.jsonify({'status': 'Using POST to upload file !'}) , 405 


@api.route('/api/delete/<filename>', methods=['GET', 'POST'])
def api_delete_file(filename):
    if libs.request.method == 'POST':

        pathfile = libs.path.join(libs.UPLOAD_FOLDER, filename)

        if libs.path.exists(pathfile):
            libs.remove(pathfile)

            return libs.jsonify({'status': 'Delete successfully !'}), 200 
        return libs.jsonify({'status': 'File not found !'}), 404 
    return libs.jsonify({'status': 'Using POST to delete file !'}), 405 
