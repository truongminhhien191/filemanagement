FROM python
LABEL maintainer="Hien Truong truongminhhien191@gmail.com"

ENV TZ 'Asia/Ho_Chi_Minh'
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /app
COPY . /app

RUN pip install Flask
RUN pip install python-dotenv

EXPOSE 5000

CMD ["python3","/app/app.py"]