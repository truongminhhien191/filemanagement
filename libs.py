from os import listdir, path, getenv, makedirs, remove
from time import ctime
from os.path import isfile, join
from flask import Flask, Blueprint, flash, request, redirect, url_for, send_from_directory, render_template, jsonify, abort
from werkzeug.utils import secure_filename
from filecmp import cmp
from dotenv import load_dotenv
import libs
from jinja2 import TemplateNotFound

APP_ROOT = path.join(path.dirname(__file__), '.')
UPLOAD_FOLDER = APP_ROOT + '/uploads'

def get_lists_file(dirname):
    listfiles = []
    stt = 0
    lists = listdir(dirname).copy()
    lists.sort(key = sort_by_date_update, reverse = True)

    for f in lists:
        stt = stt + 1
        if isfile(join(dirname, f)):
            listfiles.append(file_info(stt, f, ctime(path.getctime(join(dirname, f))), path.getsize(join(dirname, f))))

    return listfiles


def sort_by_date_update(file):
    return path.getctime(join(UPLOAD_FOLDER, file))

def sort_by_name(file):
    return file

def file_info(stt, name, date_modified, size):
    return [stt, name, date_modified, size/1000]


def is_same_name(filename, dirname):
    if filename in listdir(dirname):
        return True
    else:
        return False

def is_same_content(filename, dirname):
    pathfile = join(dirname, filename)

    for f in  listdir(dirname):
        pathitem = join(dirname, f)
        if cmp(pathfile, pathitem) and pathfile != pathitem:
            return f
    return False

